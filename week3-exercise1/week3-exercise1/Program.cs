﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week3_exercise1
{
    class Program
    {
        static void Main(string[] args)
        {
            var colors = new string[5] { "red", "blue", "orange", "white", "black" };

            for (var i = 0; i < colors.Length; i++)
            {
                Console.WriteLine(colors[i]);
            }


        }
    }
}
